# Angular-Typescript Sample App Built On Node-Express

This is a repo for a starter Anugular-Typescript Single Page MEAN Stack application. Just download and install and you have a good foundation for building application.

## Used Repos References
https://github.com/scotch-io/starter-node-angular
https://github.com/tastejs/todomvc/tree/master/examples/typescript-angular

## Pre-req
'npm install -g typescript'

## Installation
1. Download the repository
2. Install npm modules: `npm install`
3. Install bower dependencies `bower install`
4. Cd to the 'public' directory and run 'tsc --sourcemap --out js/Application.js js/_all.ts'
5. Start up the server: `node server.js`
6. View in browser at http://localhost:8080
